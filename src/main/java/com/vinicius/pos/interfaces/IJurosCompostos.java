package com.vinicius.pos.interfaces;

import java.math.BigDecimal;

import com.vinicius.pos.model.JurosCompostos;

public interface IJurosCompostos {
	
	BigDecimal calcularMontante(JurosCompostos juros);
	BigDecimal calcularTempo(JurosCompostos juros);
	BigDecimal calcularTaxa(JurosCompostos juros);
	BigDecimal calcularCapital(JurosCompostos juros);
	

}
